---
title: "價格"
date: 2018-12-28T11:02:05+06:00
icon: "ti-credit-card"
description: "保證cp值最高"
type : "pages"
---

1.單純租賃虛擬主機（每個月收100元）1200/年，檔案數不得超過10000，大小不得超過1GB

2.從原虛擬主機搬家  2000元-4000元

3.主管維護及諮詢大小問題  2000元/年

4.安裝SSL包含申請費用   8000元

5.主機優化：視情況而定(Gtmertix分數為B以上)

6.SEO：視情況而定

7.優化Pagespeed分數：視情況而定

8.資安維護（中木馬修復）：視情況而定

9.新建網站：視情況而定

10.付費CDN：依使用流量付費

11.從Pixnet搬家，視情況報價，每篇文章3-5元，20本相簿以下不另計費。

12.本公司虛擬主機只提供FTP，並未提供Cpanel登入，請考慮清楚再承租。

<center><a href="https://line.me/R/ti/p/@rid4480s" target="_blank" rel="noopener"><img class="size-full" src="https://i.imgur.com/epiisSh.png" alt="加入好友" width="232" height="72" border="0">
 

