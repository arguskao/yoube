---
author: admin
date: 2015-11-01 06:15:38+00:00
draft: false
title: Opencart和 Woocommerce的比較
type: post
categories:
- 網站建置
- 網路工具
- 行銷工具
tags:
- opencart
- woocommerce
- 購物車
---

雖然[購物車](https://www.yoube.net/tag/%e8%b3%bc%e7%89%a9%e8%bb%8a/)有很多很多種，不過對於中小企業主（也就是不想花錢的主），最常選用的就是[opencart](https://www.yoube.net/tag/opencart/)和[woocommerce](https://www.yoube.net/tag/woocommerce/)，這兩種是最常被討論到的，但對於初學者來說，到底要選擇哪種呢？各有哪些優缺點呢？

對於我來說，我自己兩種都有用，用在不一樣的販賣型態，但是業績比較好的是opencart，參考[實例](http://www.businessweekly.com.tw/KArticle.aspx?id=50527)。


## Opencart


**優點**



	  1. 檔案小，如果選用限制硬碟大小，不限制流量的[虛擬主機](https://www.yoube.net/tag/%e8%99%9b%e6%93%ac%e4%b8%bb%e6%a9%9f/)，特別適合。
	  2. 不太消耗CPU，有的虛擬主機會嚴格限制CPU資源消耗。
	  3. 上手容易，容易進入狀況

**缺點**



	  1. 外掛少，而且通常要付費
	  2. 不方便裝各種代碼，還是一樣要透過外掛，很不方便
	  3. 付費版面相對貴一點點
	  4. Google speed預設分數很低，且不容易調教。





## WOO

## 

**優點**



	  1. 外掛多，研究的人也多
	  2. 代碼容易置放，各種調教容易
	  3. 也相當容易上手
	  4. 中文化容易

**缺點**



	  1. 超級吃資源，而且硬碟空間佔據比較多。
	  2. 運行速度慢，需要比較好的虛擬主機



當然這些都是個人看法，如果有以上兩種程式問題，歡迎留言
