---
author: hotbaidu
date: 2016-07-06 12:00:10+00:00
draft: false
title: 免費JareCDN介紹-提供AWS CloudFront節點
type: post
categories:
- 網站建置
- 網路工具
tags:
- aws
- CDN
- cdn教學
- SSL
- 免費
- 網站建置
- 網路工具
- 雲端
---

JareCDN是今年推出的一項CDN加速服務，
結合[Amazon CloudFront](https://aws.amazon.com/tw/cloudfront/)服務，可以取代去年停止服務的[Firesize.com](https://free.com.tw/firesize/)，
但一樣建議用來加速圖片，若用在css跟js加速可能會出現錯誤。

JareCDN官網 [http://www.jare.io](http://www.jare.io)

[![2freecdntes](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes.jpg)
](http://www.jare.io)

首先點選上方的[登入](https://github.com/login/oauth/authorize?client_id=892640825fd7f81b651e&redirect_uri=http%3A%2F%2Fwww.jare.io%2F%3FPsByFlag%3DPsGithub)鈕。

[![2freecdntes1](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes1.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes1.jpg)

使用[GitHub帳號](https://github.com/login/oauth/authorize?client_id=892640825fd7f81b651e&redirect_uri=http%3A%2F%2Fwww.jare.io%2F%3FPsByFlag%3DPsGithub)綁定。

[![2freecdntes2](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes2.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes2.jpg)

登入後，點選domains設定要加速的網域。

[![2freecdntes3](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes3.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes3.jpg)

添加網域(不需要http//或https://開頭)。

[![2freecdntes4](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes4.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes4.jpg)

以下是JareCDN的網址規則。

[![2freecdntes5](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes5.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes5.jpg)

使用JareCDN的網址範例如下：(請將yourdomains修改成您的網域)

    
    https://cf.jare.io/?u=http://yourdomains


或

    
    https://djk1be5eatcae.cloudfront.net/?u=http://yourdomains
    


JareCDN刷新緩存的路徑範例如下：(請使用完整檔案路徑，請將yourdomains修改成您的網域)

    
    https://relay.jare.io/?u=http://yourdomains


了解規則後，請將CDN路徑配置到快取外掛的CDN選項。

建議只用來加速圖片，可排除以下副檔名的字串

    
    .php,.htaccess,.html,.js,.css,.txt,.xml


[![2freecdntes6](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes6.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/2freecdntes6.jpg)

刷新快取外掛內容後即可測試JareCDN是否生效。
