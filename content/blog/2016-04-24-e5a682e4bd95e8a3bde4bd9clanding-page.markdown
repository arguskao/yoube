---
author: admin
date: 2016-04-24 02:18:20+00:00
draft: false
title: 如何製作Landing Page
type: post
categories:
- 行銷工具
tags:
- 一頁式商店
---

這幾年LangingPage很流行，也有人稱一頁式商店，個人覺得非常適合剛開始踏入電商領域的初學者，接下來就跟大家介紹一下。


## 收費


[一頁式商店](http://www.super-landing.com)，雖然要收費，我自己也沒試過，但是從本身介紹的內容來說，算是比Shopline這些好用，大家可以試試看。

[unbounce](http://unbounce.com/)，國際上相當有名氣，功能相當強，已經有做好的模版可以選，功能也很強，[這裡有介紹](https://www.dcplus.com.tw/market/unbounce)。只是價格相當不親民，最基礎的每月49美元，只能允許每月5000人瀏覽，天呀，這廣告一打，也不只了吧。如果真能負擔起每月199美金，每年七八萬的預算，大概也會想要客製化自己的網站吧。

[blocsapp](http://blocsapp.com/)，網路上介紹的比較少，可以免費試用五天，購買正式版個人69.99美金，公司則要299美金。




## 免費


[squarespace](https://www.squarespace.com/)，可以直接挑非常有設計感的模版，不過好像不能用自己的網址。

[launchrock](https://www.launchrock.com)，一樣是挑模版，如果免費版只能用它提供的網址，而且不能自定HTML&CSS，不然就付費版，每個月5美金。 

[Striking.ly](https://www.striking.ly)，這裡有[詳細的介紹](http://julia00.blogspot.tw/2012/10/strikingly.html)，免費版用戶可以製作兩個網站，如果想要有自訂網址，一樣得付費，可選擇一個月8塊美金或者16塊美金方案。創辦人是華裔，中文的支援度比較好。


