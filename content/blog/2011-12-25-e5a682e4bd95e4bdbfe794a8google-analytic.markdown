---
author: admin
date: 2011-12-25 14:37:37+00:00
draft: false
title: 如何使用Google Analytic
type: post
categories:
- 網路行銷課程
tags:
- google
- seo
---

在google的許多工具裡面，大家最常用到的一定是搜尋，然後是他的gmail，但是對我來說，google的[分析工具](http://www.google.com/intl/zh-TW/analytics/)，絕對是[SEO](http://www.yoube.net/tag/seo/)工作者不可或缺的好幫手。

一般人在寫Blog時候，常常會自己寫的很開心，但是沒有人看，大概兩三個禮拜沒人回應以後，就會自動慢慢消失了。但是如果能夠透過google提供的工具，就可以知道為何我們寫的東西沒有人看，讓我們可以好好修正。

這一篇是教你[如何申請帳號](http://taciasi.pixnet.net/blog/post/66690357-google-analytics(%E5%88%86%E6%9E%90)-ch1-%E5%85%A5%E9%96%80%E6%95%99%E5%AD%B8%E7%94%B3%E8%AB%8Banalytics%E5%8F%8Ag)。申請帳號以後，記得貼到你所申請的網站中，不同的網站有不同的貼法，當然最方便的就是 Google自己提供的[Blogspot](http://googleblog.blogspot.tw/)服務。那如果用 pixnet的人，可以看[這一篇](http://www.soft4fun.net/network/%E7%B6%B2%E8%B7%AF%E7%9B%B8%E9%97%9C-%E5%9C%A8pixnet%E7%97%9E%E5%AE%A2%E5%B9%AB%E7%B6%B2%E8%AA%8C%E7%B3%BB%E7%B5%B1%E5%8A%A0%E5%85%A5%E5%A5%BD%E7%8E%A9%E6%84%8F%E7%9A%84%E6%96%B9%E6%B3%95.htm)。我自己是用wordpress，[安裝起來也是相當方便](http://sofree.cc/google-analytics/)，不過要進入後台來改。

申請以後，要等一兩天以後，就可以進入後台，看看我們的資料有什麼可以利用的呢？打仗一定要知己知彼，使用Google analytic就是知彼，雖然使用上有點複雜，還是請大家一定要申請，申請好以後，請留言給我，繼續進入下一課。

延伸閱讀：[自定報告](http://portable.easylife.tw/1700)。[這裡還有](http://blog.xuite.net/yh96301/blog/52168354-Google+Analytics%E7%B5%B1%E8%A8%88%E5%92%8C%E5%88%86%E6%9E%90%E9%83%A8%E8%90%BD%E6%A0%BC%E7%9A%84%E6%B5%81%E9%87%8F)

下一篇[如何申請adsense](http://www.yoube.net/2011/12/20/%e5%a6%82%e4%bd%95%e7%94%b3%e8%ab%8badsense/)


