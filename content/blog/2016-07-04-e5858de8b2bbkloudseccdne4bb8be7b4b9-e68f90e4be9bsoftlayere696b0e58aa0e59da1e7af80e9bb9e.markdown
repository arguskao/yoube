---
author: hotbaidu
date: 2016-07-04 13:05:34+00:00
draft: false
title: 免費KloudsecCDN介紹-提供Softlayer新加坡節點
type: post
categories:
- 網站建置
- 網路工具
tags:
- CDN
- cdn教學
- SSL
- 免費
- 有限流量
- 網路工具
- 雲端
---

Kloudsec是今年才推出的CDN服務，
與KeyCDN一樣使用Softlayer伺服器資源建設節點，
目前節點包括英國、美國、新加坡，
與[CloudFlare](https://www.hzsh.xyz/173/cloudflare%e5%8a%a0%e9%80%9f%e6%9c%80%e4%bd%b3%e5%8c%96%e8%a8%ad%e7%bd%ae)一樣使用Anycast所有節點IP相同，
不須修改網域NS紀錄就可以使用CDN，
並且免費提供SSL憑證安裝或搭配Let's Encrypt簽發功能，
補足Incapsula免費版無法使用SSL的麻煩。

Kloudsec官網 [https://kloudsec.com](https://kloudsec.com)

[![Kloudsec](https://www.yoube.net/wp-content/uploads/2016/07/Kloudsec.jpg)
](https://kloudsec.com)

<!-- more -->在官網按下[preview dashboard](https://www.kloudsec.com/dashboard/websites/preview)申請服務。

[![freecdntes1](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes1.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes1.jpg)

輸入要設定CDN的域名。

[![freecdntes2](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes2.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes2.jpg)

進入Kloudsec控制面板，請先驗證DNS配置。

[![freecdntes3](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes3.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes3.jpg)

分別修改指定A記錄到Kloudsec提供的IP，完成後按下驗證。
(剛開始可能會驗證失敗，不用重新設定，系統將會在DNS生效後自動完成驗證)

[![freecdntes4](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes4.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes4.jpg)

Speed頁面可以打開頁面優化。

[![freecdntes5](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes5.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes5.jpg)

Kloudsec在這裡提供頁面優化的等級說明，通常選Basic等級即可。

[![freecdntes6](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes6.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes6.jpg)

Reliability可以設定離線緩存頁面。

[![freecdntes7](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes7.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes7.jpg)

選擇免費方案的離線緩存(只有單頁面緩存)。

[![freecdntes8](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes8.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes8.jpg)

完成離線緩存配置(因為只有單頁面，站點當機時幫助有限)。

[![freecdntes9](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes9.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes9.jpg)

Protection頁面可以啟動網域Let's Encrypt簽發功能。

[![freecdntes10](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes10.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes10.jpg)

開始簽發網域(可能需要等待一段時間才會完成)。

[![freecdntes11](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes11.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes11.jpg)

點選左邊面板的網域，設定網域的SSL配置。

[![freecdntes12](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes12.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes12.jpg)

包括自動重新導向HTTPS、自動重寫內容到HTTPS、使用HTTPS(443)訪問源站等功能。

[![freecdntes13](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes13.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes13.jpg)

至於Web Shield功能尚未成熟，不建議使用。

[![freecdntes14](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes14.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes14.jpg)

站點配置可以修改源站IP或CNAME。

[![freecdntes15](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes15.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/freecdntes15.jpg)

Kloudsec很適合搭配[新版360網站衛士](https://www.hzsh.xyz/895/%e4%b8%ad%e5%9c%8b%e5%85%8d%e8%b2%bb%e4%b8%8d%e9%99%90%e6%b5%81%e9%87%8fcdn%e4%bb%8b%e7%b4%b9-360%e7%b6%b2%e7%ab%99%e8%a1%9b%e5%a3%ab-%e7%99%be%e5%ba%a6%e9%9b%b2%e5%8a%a0%e9%80%9f-%e7%89%9b%e7%9b%be)做為海外CDN，
讓網站在海內外都能透過近端CDN節點快取提高訪問性能。
