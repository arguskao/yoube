---
author: admin
date: 2015-02-19 03:58:22+00:00
draft: false
title: 美國虛擬主機Interserver
type: post
categories:
- 虛擬主機
- 銷售秘技
tags:
- 美國虛擬主機
- 虛擬主機
---

最近又要找尋夠力的[虛擬主機](https://www.yoube.net/tag/%e8%99%9b%e6%93%ac%e4%b8%bb%e6%a9%9f/)，看到網路上有人[推薦interserver的主機](http://sofun.tw/interserver/)，剛好看到有優惠碼，首月只要0.01元，想說就算不好用，也可以測試看看，於是就申請了。

[![螢幕截圖_2015-02-19_11_46_29](https://www.yoube.net/wp-content/uploads/2015/02/2015-02-19_11_46_29.jpg)
](https://www.yoube.net/wp-content/uploads/2015/02/2015-02-19_11_46_29.jpg)

[![螢幕截圖_2015-02-19_11_46_39](https://www.yoube.net/wp-content/uploads/2015/02/2015-02-19_11_46_39.jpg)
](https://www.yoube.net/wp-content/uploads/2015/02/2015-02-19_11_46_39.jpg)

從探針的資訊看來，這樣的主機規格真是掉漆呀......

繼續試試看，真正架站起來會怎樣。

他們的主機比較特別，用的是LiteSpeed，既不是Nginx，也不是Apache，後台的速度不夠快，卡卡的。唯一的好處就是空間和流量無限。但是一個月將近5元美金的價格，我看意義也不大。

匯入資料庫時候，速度也非常慢，幾乎是一般主機的三四倍時間，都快睡著了。最後匯入後還有點錯誤，也懶得再重架了。


<blockquote>結論，台灣民眾最好不要使用這個主機</blockquote>




下一篇[創業補助資源列表](https://www.yoube.net/2014/12/16/%e5%89%b5%e6%a5%ad%e8%a3%9c%e5%8a%a9%e8%b3%87%e6%ba%90%e5%88%97%e8%a1%a8/)
