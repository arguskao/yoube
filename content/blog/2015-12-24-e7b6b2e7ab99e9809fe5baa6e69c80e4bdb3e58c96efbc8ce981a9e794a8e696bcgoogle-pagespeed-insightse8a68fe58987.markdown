---
author: hotbaidu
date: 2015-12-24 16:25:24+00:00
draft: false
title: 網站速度最佳化，適用於Google PageSpeed Insights規則
type: post
categories:
- 網站建置
- 網路工具
- 虛擬主機
tags:
- CDN
- google analytics
- seo
- SSL
- wordpress
- 免費
- 網路工具
---

網站的載入速度往往影響到訪客瀏覽的意願，
大部分站長會選擇針對[PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/)系統提供的[規則](https://developers.google.com/speed/docs/insights/rules)進行優化，
整理評分範圍0~100之間，達到85分表示網頁載入的性能良好，
並針對各項規則分別列出綠色良好、黃色注意、紅色警告的信息，
以下為本站點針對[WordPress](https://zh-tw.wordpress.com/)優化的方案提供參考。

**PageSpeed Insights官網** [https://developers.google.com/speed/pagespeed/insights/](https://developers.google.com/speed/pagespeed/insights/)



[![pagespeed](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed.jpg)
](https://developers.google.com/speed/pagespeed/insights/)<!-- more -->

**第一項：修改jQuery公共庫引用來源**

考量到國內外訪客的瀏覽速度，選擇在全球與中國各地都有CDN節點的公共庫可以有效提升預設jQuery載入延遲的瓶頸。(以下提供的公共庫均支持https訪問)

BootCDN中文官網(有大量海內外節點) [http://www.bootcdn.cn/](http://www.bootcdn.cn/) 提供SPDY3.1連線

又拍雲公共庫官網(有大量海內外節點) [http://jscdn.upai.com/](http://jscdn.upai.com/) 提供SPDY3.1連線

jsDelivr官網(有大量海外節點) [https://www.jsdelivr.com/](https://www.jsdelivr.com/) 提供HTTP/2連線

可搭配切換jQuery公共庫引用外掛：Switch jQuery Version

Switch jQuery Version外掛頁面 [https://wordpress.org/plugins/switch-jq-version/](https://wordpress.org/plugins/switch-jq-version/)

詳細配置請參考這篇：又拍雲JS公共庫

[![pagespeed1](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed1.jpg)
](https://wordpress.org/plugins/switch-jq-version/)

**第二項：停用佈景主題預設引用Google字形**

建議另外修改佈景的CSS字形，較能相容各個平台。

Remove Google Fonts References外掛頁面 [https://wordpress.org/plugins/remove-google-fonts-references/](https://wordpress.org/plugins/remove-google-fonts-references/)



[![pagespeed3](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed3.jpg)
](https://wordpress.org/plugins/remove-google-fonts-references/)

**第三項：針對多媒體素材使用LazyLoad延遲載入**

對於頁面載入的同時，若大量載入所有圖片、影片容易造成訪客畫面停頓，
瀏覽體驗容易受影響，建議針對多媒體啟用LazyLoad。

a3 Lazy Load外掛頁面 [https://wordpress.org/plugins/a3-lazy-load/](https://wordpress.org/plugins/a3-lazy-load/)



[![pagespeed2](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed2.jpg)
](https://wordpress.org/plugins/a3-lazy-load/)

a3 Lazy Load外掛插件安裝後，直接啟用LazyLoad配置即可。

[![pagespeed21](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed21.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed7.jpg)

這裡可以選擇Script載入位置與讀取圖片的效果，
並建議設置Threshold為600~900px，讓訪客比較不會感受到圖片載入延遲的情形。

[![pagespeed22](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed22.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed22.jpg)

如果站長使用評論外掛插件使用[Disqus](https://disqus.com/)，建議可以改用[Disqus Conditional Load外掛](https://wordpress.org/plugins/disqus-conditional-load/)取代[Disqus官方外掛](https://disqus.com/admin/wordpress/)，讓Disqus可以LazyLoad。(使用本外掛前要移除原本的Disqus官方外掛)

Disqus Conditional Load外掛頁面 [https://wordpress.org/plugins/disqus-conditional-load/](https://wordpress.org/plugins/disqus-conditional-load/)

**第四項：使用快取緩存外掛**

緩存外掛可以同時降低訪客訪問頁面的反應時間與伺服器消耗的資源，並同時提供HTML優化的效果。

W3 Total Cache外掛頁面 [https://wordpress.org/plugins/w3-total-cache/](https://wordpress.org/plugins/w3-total-cache/)



[![pagespeed4](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed4.jpg)
](https://wordpress.org/plugins/w3-total-cache/)

要先到General設定啟用所需的壓縮功能與瀏覽器緩存功能，其他配置請自訂。

[![pagespeed41](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed41.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed41.jpg)

啟用壓縮HTML的配置。

[![pagespeed42](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed42.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed42.jpg)

啟用CSS與JS的瀏覽器緩存配置，會直接修改伺服器的htaccess檔案。(建議設定一年)

[![pagespeed43](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed43.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed43.jpg)

啟用多媒體素材的瀏覽器緩存配置，同樣會修改伺服器的htaccess檔案。(建議設定一年)

[![pagespeed44](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed44.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed44.jpg)

通常快取外掛同時會有CDN功能，建議可以配置子網域給CDN使用強制快取的設置，
不過針對https網站來說，建議要到htaccess配置301或302強制https訪問，
先前測試時若訪問http網頁後，改瀏覽https容易出現不安全的混合內容
(根據Google說法，網頁有混合內容的安全性等同沒有使用https，Google不會優先收錄)

強制跳轉到https的htaccess配置

    
    RewriteEngine on



    
    # Uncomment the following lines to force HTTPS
    RewriteCond %{HTTP:X-Forwarded-Proto} !https
    RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [R,L]


**第五項：優化JS與CSS載入方式**

這裡推薦使用[Autoptimize外掛插件](https://wordpress.org/plugins/autoptimize/)，可以有效縮小JS、CSS大小，並解決[PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/)常見[移除禁止轉譯](https://developers.google.com/speed/docs/insights/BlockingJS)的問題。另外會針對每個頁面產生對應所有CSS、JS合併緩存的內容，這部分未來[HTTP/2](https://zh.wikipedia.org/wiki/HTTP/2)盛行之後應該會取消，強化瀏覽器對於不同頁面相同素材的304緩存效果。

Autoptimize外掛頁面 [https://wordpress.org/plugins/autoptimize/](https://wordpress.org/plugins/autoptimize/)



[![pagespeed5](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed5.jpg)
](https://wordpress.org/plugins/autoptimize/)

預設提供精簡配置，請先切換成進階設定。

![pagespeed51](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed51.jpg)


啟用JavaScript優化。

![pagespeed52](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed52.jpg)


啟用CSS優化，並建議內聯(Inline and Defer CSS)您使用的佈景主題CSS到HTML中，
可搭配CSS Compressor [http://csscompressor.com/](http://csscompressor.com/) 可去除CSS多餘資訊並壓縮，
再貼上Inline and Defer CSS選項附加到HTML中。

[![pagespeed53](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed53.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed53.jpg)

**第六項：本地緩存GoogleAnalytics的JS，並使用伺服器cron配置自動更新GoogleAnalytics緩存**

這部分比較可惜的是[Autoptimize外掛](https://wordpress.org/plugins/autoptimize/)無法直接合併analytics.js，故另外配置PHP緩存與刷新analytics.js規則。
請參考文章 [http://diywpblog.com/leverage-browser-cache-optimize-google-analytics/](http://diywpblog.com/leverage-browser-cache-optimize-google-analytics/)
將// script to update local version of Google analytics script的部分另存成.php檔案。

[![pagespeed6](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed6.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed6.jpg)

修改$romoteFile=[https://www.google-analytics.com/analytics.js](https://www.google-analytics.com/analytics.js)

而$localfile=請寫上欲存放analytics.js的區域，並確保這個地方的analytics.js檔案可以被讀取與寫入。

[![pagespeed61](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed61.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed61.jpg)

再來就是先複製好.php檔案在網頁伺服器的存放路徑(通常經銷商主機的存放路徑跟上圖大同小異)，
請進入貴站點的主機操作面板(這裡以Hostinger的自建面板為例)。

[![pagespeed62](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed62.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed62.jpg)

選擇Cron工具，創建Cron排程規則，寫上運行命令跟選擇常用選項的每天一次即可。
但通常初次建議先選擇建立每分鐘，先產生analytics.js，再移除規則重設為每天一次。

[![pagespeed63](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed63.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed63.jpg)

檢測您網站的.php是否出現錯誤信息，如果沒有應該會直接輸出成與analytics.js相同內容的頁面，
再去刷新WordPress外掛快取，即可顯示出網站本地的analytics.js資訊。

[![pagespeed64](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed64.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed64.jpg)

上述配置完成後就能到佈景主題的header.php將原本的//www.google-analytics.com/analytics.js
修改成本地配置的路徑，刷新快取緩存後即可到GoogleAnalytics檢測配置是否正確無誤。

[![pagespeed65](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed65.jpg)
](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed65.jpg)

**第七項：使用圖片最佳化程式先壓縮網頁圖片再上傳到WordPress媒體庫**

圖片最佳化可透過阿榮福利味提供的FileOptimizer程式(Windows系統適用)進行批次壓縮

FileOptimizer阿榮福利味 [http://www.azofreeware.com/2014/04/fileoptimizer-600281.html](http://www.azofreeware.com/2014/04/fileoptimizer-600281.html)



[![pagespeed7](https://www.yoube.net/wp-content/uploads/2015/12/pagespeed7.jpg)
](http://www.azofreeware.com/2014/04/fileoptimizer-600281.html)

FileOptimizer壓縮後會自動將舊檔案(未壓縮)移動到Windows回收桶，
如果要保留原始檔案建議要先備份會比較保險！

以上的方法提供給站長作為速度優化的參考，
建議可以搭配常見的CDN服務優化網站與Google的連線速度，可以有效[加快伺服器回應時間](https://developers.google.com/speed/docs/insights/Server)的要求，
非https網站優先參考節點線路較佳的Incapsula [https://www.incapsula.com](https://www.incapsula.com/)
而https網站則使用CloudFlare [https://www.cloudflare.com](https://www.cloudflare.com/)
