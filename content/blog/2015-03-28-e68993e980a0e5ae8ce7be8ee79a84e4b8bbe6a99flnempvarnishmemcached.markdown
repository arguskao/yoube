---
author: admin
date: 2015-03-28 01:31:57+00:00
draft: false
title: 打造完美的主機LNEMP+Varnish+Memcached
type: post
categories:
- 網站建置
- 虛擬主機
tags:
- wordpress
- 虛擬主機
---

經過這段時間的研究，發現要打造一台速度快又能負擔不大的主機，除了[安裝Nginx](https://www.yoube.net/2015/03/04/%E5%9C%A8ubuntu%E4%B8%8B%E5%AE%89%E8%A3%9Dnginx/)以外，前端的Varnish也很重要，安裝varnish教學已經很多，就不多說，主要就是把Varnish設定80 port，然後Nginx改成8080 port，這樣的話，可以降低CPU的loading，如果是使用wordpress，因為是偽靜態，所以使用Memcached，可以加快速度。

但是如果是Ghost的Blog system，就不知道Memcahed效果好不好了。



下一篇[新加坡主機](https://www.yoube.net/2015/03/17/%e6%96%b0%e5%8a%a0%e5%9d%a1%e4%b8%bb%e6%a9%9f/)
