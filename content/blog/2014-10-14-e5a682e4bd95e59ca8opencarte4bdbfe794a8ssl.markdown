---
author: admin
date: 2014-10-14 07:02:59+00:00
draft: false
title: 如何在Opencart使用SSL
type: post
categories:
- 網站建置
tags:
- opencart
- SSL
---

上一篇文章提到[如何安裝SSL證書](https://www.yoube.net/2014/07/05/%E5%A6%82%E4%BD%95%E5%AE%89%E8%A3%9Dssl%E8%AD%89%E6%9B%B8/)以後，大家不知道有沒有到處去試試看呢？

其實一般的網站，例如像部落格，是沒有必要裝SSL的，但是電子商務網站就必須要加裝[SSL](https://www.yoube.net/tag/ssl/)，我自己習慣用的[opencart](https://www.yoube.net/tag/opencart/)，要加裝SSL也很容易。



	  1. 先安裝好SSL證書
	  2. 然後進入後台，在網站設定那裏，最右邊的Server，裡面有個要使用SSL，選是
	  3. 然後進入系統的地方，修改Config.php，把程式裡面原來HTTPS，卻沒有使用https，都改成https
	  4. 最後請加上一個.htaccess，強迫轉向https，不然除了主頁有https以外，其他地方是不會有的。



下一篇[如何讓影片變成GIF](https://www.yoube.net/2014/10/11/%e5%a6%82%e4%bd%95%e5%b0%87%e5%bd%b1%e7%89%87%e8%ae%8a%e6%88%90gif/)
