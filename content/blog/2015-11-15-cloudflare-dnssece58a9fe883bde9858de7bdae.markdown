---
author: hotbaidu
date: 2015-11-15 04:04:09+00:00
draft: false
title: CloudFlare DNSSEC功能配置
type: post
categories:
- 網路工具
tags:
- CDN
- cdn教學
- 免費
- 網路工具
---

CloudFlare DNSSEC官網 [https://www.cloudflare.com/dnssec/](https://www.cloudflare.com/dnssec/)

[![dnssec](https://www.yoube.net/wp-content/uploads/2015/11/dnssec.jpg)
](https://www.cloudflare.com/dnssec/)

2015年11月，CloudFlareDNS推出DNSSEC功能，用來驗證網域跟IP之間的正確性，解決域名容易受到DNS污染的問題，一方面也能降低域名被用來DNS放大攻擊的可能性。<!-- more -->

設定CloudFlareDNS的DNSSEC，請先點選CloudFlare面板上的DNS功能選單

![dnssec1](https://www.yoube.net/wp-content/uploads/2015/11/dnssec1.jpg)


頁面下方會看到DNSSEC選項，請點選"Enable DNSSEC"開始配置

[![dnssec2](https://www.yoube.net/wp-content/uploads/2015/11/dnssec2.jpg)
](https://www.yoube.net/wp-content/uploads/2015/11/dnssec2.jpg)

此時會出現DNSSEC的DS紀錄與其他相關數據，請提交到網域註冊商進行設定

[![dnssec3](https://www.yoube.net/wp-content/uploads/2015/11/dnssec3.jpg)
](https://www.yoube.net/wp-content/uploads/2015/11/dnssec3.jpg)

以下是目前提供DNSSEC設定的註冊商列表，並提供相關配置說明

網域註冊商配置列表 [https://support.cloudflare.com/hc/en-us/articles/209114378](https://support.cloudflare.com/hc/en-us/articles/209114378)

[![dnssec4](https://www.yoube.net/wp-content/uploads/2015/11/dnssec4.jpg)
](https://support.cloudflare.com/hc/en-us/articles/209114378)

部分註冊商已經提供自動化的方式配置DNSSEC，但筆者的註冊商NameCheap則需要聯絡即時客服人員請他們開工單設定DNSSEC。

以下是CloudFlareDNS的DNSSEC配置成功的狀態

[![dnssec5](https://www.yoube.net/wp-content/uploads/2015/11/dnssec5.jpg)
](https://www.yoube.net/wp-content/uploads/2015/11/dnssec5.jpg)

此時可以透過第三方檢測工具測試DNSSEC狀態

ViewDNS Tools [http://viewdns.info/dnssec/](http://viewdns.info/dnssec/)

[![dnssec6](https://www.yoube.net/wp-content/uploads/2015/11/dnssec6.jpg)
](http://viewdns.info/dnssec/)

此時，可以檢測常見Public DNS的DNSSEC生效狀態，在Linux有很好用的dig工具，如果是Windows用戶可以安裝套件也能使用dig工具。

BIND下載官網 [http://www.bind9.net/download](http://www.bind9.net/download)

[![dnssec7](https://www.yoube.net/wp-content/uploads/2015/11/dnssec7.jpg)
](http://www.bind9.net/download)

可以看到官網上面有很多鏡像載點，但是大多已經失效，以下整理還可以用的載點

日本 [ftp://ftp.iij.ad.jp/pub/network/isc/bind9/9.9.8/](ftp://ftp.iij.ad.jp/pub/network/isc/bind9/9.9.8/)
美國 [ftp://ftp.nominum.com/pub/isc/bind9/](ftp://ftp.nominum.com/pub/isc/bind9/)
美國 [ftp://ftp.isc.org/isc/bind9/](ftp://ftp.isc.org/isc/bind9/)

請點選最新版本號，目前最新正式版為9.9.8

[![dnssec8](https://www.yoube.net/wp-content/uploads/2015/11/dnssec8.jpg)
](https://www.yoube.net/wp-content/uploads/2015/11/dnssec8.jpg)

請依照Windows系統下在x86或x64，這裡以x86示範

[![dnssec9](https://www.yoube.net/wp-content/uploads/2015/11/dnssec9.jpg)
](https://www.yoube.net/wp-content/uploads/2015/11/dnssec9.jpg)

下載時可能會被Chrome認為是罕見檔案需手動解除封鎖，並請解壓縮檔案

[![dnssec10](https://www.yoube.net/wp-content/uploads/2015/11/dnssec10.jpg)
](https://www.yoube.net/wp-content/uploads/2015/11/dnssec10.jpg)

請先安裝資料夾中"vcredist_x86.exe"(這是x86版本的)

並將資料夾中的.dll應用程式擴充檔全部複製

[![dnssec11](https://www.yoube.net/wp-content/uploads/2015/11/dnssec11.jpg)
](https://www.yoube.net/wp-content/uploads/2015/11/dnssec11.jpg)

打開Windows的System32資料夾貼上.dll應用程式擴充檔

[![dnssec12](https://www.yoube.net/wp-content/uploads/2015/11/dnssec12.jpg)
](https://www.yoube.net/wp-content/uploads/2015/11/dnssec12.jpg)

上述步驟操作完後，開啟cmd(Win+R執行cmd或搜尋cmd開啟)

鍵入dig 網域名稱 +dnssec @要測試的PublicDNS的IP

範例 dig paypal.com +dnssec @1.2.4.8

可以查詢到CNNIC SDNS上PayPal域名的RRSIG紀錄表示支援DNSSEC並且生效

目前測試Google PublicDNS、Hinet PublicDNS、CNNIC SDNS、百度 BaiduDNS均支持DNSSEC，這對於DNS安全有很大的幫助。

[![dnssec13](https://www.yoube.net/wp-content/uploads/2015/11/dnssec13.jpg)
](https://www.yoube.net/wp-content/uploads/2015/11/dnssec13.jpg)


