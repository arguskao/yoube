---
author: hotbaidu
date: 2016-03-28 15:49:17+00:00
draft: false
title: VestaCP安裝-免費VPS控制面板配置教學
type: post
categories:
- 網站建置
- 網路工具
- 虛擬主機
tags:
- VPS
- 免費
- 網路工具
- 雲端
---

[VestaCP](https://vestacp.com/)提供VPS使用者簡易的網頁主機環境，
透過簡單的指令建立LNAMP+電子郵件系統，
性能優異加上圖形化管理介面方便性高，
常被當作cPanel系統的替代方案。
但是實際安裝操作下，[VestaCP](https://vestacp.com/)有諸多細節須留意，
才能順利配置站點到[VestaCP](https://vestacp.com/)系統運行。

VestaCP官網 [https://vestacp.com/](https://vestacp.com/)

[
](https://vestacp.com/)[![vestacpip2](https://www.yoube.net/wp-content/uploads/2016/03/vestacpip2.jpg)
](https://hzsh.xyz/679/vestacp安裝-免費vps控制面板配置教學)
<!-- more -->

[VestaCP](https://vestacp.com/)支持以下作業系統

    
    RHEL/CentOS 5,6,7 
    Debian 6,7,8 
    Ubuntu 12.04-15.10




## 1.下載[VestaCP](https://vestacp.com/)腳本並執行安裝


準備好VPS或獨立主機的SSH終端連線後，進入[VestaCP](https://vestacp.com/)官網，

點選[Install](https://vestacp.com/#install)，顯示[Install](https://vestacp.com/#install)標籤的內容

[![vestacpi1](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi1.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi1.jpg)



[![vestacpi2](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi2.jpg)
](https://hzsh.xyz/679/vestacp安裝-免費vps控制面板配置教學)

此時請先確認主機已經安裝curl，即可透過指令

    
    curl -O http://vestacp.com/pub/vst-install.sh


下載安裝腳本到主機，

再往下捲動頁面到Advanced Install Settings

選擇站長所需的系統環境

[![vestacpi3](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi3.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi3.jpg)

如果不需要信箱系統可以取消，將能節省主機RAM的消耗

[![vestacpi4](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi4.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi4.jpg)

自訂系統選擇好後按下Generate Install Command

會出現bash開頭的指令即可貼上終端

[![vestacpi5](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi5.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi5.jpg)

終端機顯示以下對話確認後執行[VestaCP](https://vestacp.com/)安裝

[![vestacpi6](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi6.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi6.jpg)

大約10分鐘左右完成[VestaCP](https://vestacp.com/)安裝，

開啟瀏覽器開啟 https://伺服器的IP:8083

輸入帳號與密碼登入管理員介面

[![vestacpi7](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi7.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi7.jpg)


## 2.修復[VestaCP](https://vestacp.com/)的資料庫與phpMyAdmin串接問題


由於目前這個[VestaCP](https://vestacp.com/)版本會出現phpMyAdmin無法連接資料庫的問題

請先執行以下指令(由[cloudwp](https://cloudwp.pro/blog/vesta-%E8%BF%91%E4%B9%8E%E5%AE%8C%E7%BE%8E%E7%9A%84%E5%85%8D%E8%B2%BB-vps-%E9%9D%A2%E6%9D%BF/)提供的教學)

    
    cd /usr/share/doc/phpmyadmin/examples



    
    gunzip config.sample.inc.php.gz



    
    mv ./config.sample.inc.php /etc/phpmyadmin/config.inc.php


[![vestacpi8](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi8.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi8.jpg)

或透過[WinSCP](http://www.azofreeware.com/2008/03/winscp-41-beta.html)以SFTP連接主機([按這裡前往阿榮福利味下載程式](http://www.azofreeware.com/2008/03/winscp-41-beta.html))，

將/usr/share/doc/phpmyadmin/examples底下的

config.sample.inc.php複製到本地電腦改名為config.inc.php

[![vestacpi9](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi9.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi9.jpg)

再上傳到/etc/phpmyadmin/覆蓋原始檔案即可

[![vestacpi10](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi10.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi10.jpg)


## 3.安裝繁體中文語言包


接下來安裝繁體中文的語言包到[VestaCP](https://vestacp.com/)，請在終端輸入

    
    wget https://raw.githubusercontent.com/serghey-rodin/vesta/master/web/inc/i18n/tw.php -O /usr/local/vesta/web/inc/i18n/tw.php


[![vestacpi11](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi11.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi11.jpg)

登入[VestaCP](https://vestacp.com/)管理員面板

[![vestacpi12](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi12.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi12.jpg)

先點選管理員帳戶更改語言

[![vestacpi13](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi13.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi13.jpg)

Language選擇tw並保存，即可出現繁體中文操作介面

[![vestacpi14](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi14.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi14.jpg)

接著，點選"服務器"選項

[![vestacpi15](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi15.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi15.jpg)

點選齒輪圖示配置伺服器

[![vestacpi16](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi16.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi16.jpg)

更改伺服器預設時區與預設語言

[![vestacpi17](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi17.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi17.jpg)

配置伺服器的備份數量與備份位置(提供FTP/SFTP遠程備份的功能)

[![vestacpi18](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi18.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi18.jpg)


## 4.啟用[VestaCP](https://vestacp.com/)自動更新


為了降低[VestaCP](https://vestacp.com/)未及時更新造成伺服器空窗問題，請點選"更新"

[![vestacpi19](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi19.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi19.jpg)

啟用自動更新，讓[VestaCP](https://vestacp.com/)自動升級到最新的系統

[![vestacpi20](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi20.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi20.jpg)


## 5.設定不同方案規格並建立子帳戶


[VestaCP](https://vestacp.com/)面板提供多帳戶管理的服務(類似WHM)，

點選"套件"，在某個方案按下"編輯"即可調整不同方案的規格

包括網頁主機空間、流量、資料庫數量、備份量，以及是否開放SSH連線

[![vestacpi21](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi21.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi21.jpg)

配置好套件方案的規格後，即可新增[VestaCP](https://vestacp.com/)的子帳戶，

請點選"使用者"並按下增加的圖示即可建立

[![vestacpi22](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi22.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi22.jpg)


## 6.配置網站的SSL


站點SSL已經是網站基本配備，

[VestaCP](https://vestacp.com/)網站安裝需要安裝SSL時點選"網頁"，新增或編輯網域對應配置

[![vestacpi23](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi23.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi23.jpg)

勾選SSL功能，選擇好SSL家目錄後分別貼上證書代碼

[![vestacpi24](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi24.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi24.jpg)

若使用Apache格式的SSL證書，

請務必補齊SSL中級憑證避免斷裂問題，完成後儲存即可

[![vestacpi25](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi25.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi25.jpg)


## 7.配置[VestaCP](https://vestacp.com/)面板的SSL


網頁的SSL配置可以透過[VestaCP](https://vestacp.com/)面板進行處理，

但是[VestaCP](https://vestacp.com/)面板本身的SSL需要上傳到

/usr/local/vesta/ssl/底下

務必使用Nginx格式的SSL憑證，

若使用Apache格式SSL憑證時，

請將中級憑證加入網域憑證中串接再上傳到伺服器上覆蓋

certificate.crt
certificate.key

[![vestacpi26](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi26.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi26.jpg)

確認[VestaCP](https://vestacp.com/)面板可透過https訪問表示SSL已經生效


## 8.修改[VestaCP](https://vestacp.com/)面板的連接阜


針對[VestaCP](https://vestacp.com/)面板的安全性來說，可以修改port提高安全性

請點選"防火牆"，按下/VESTA編輯

[![vestacpi27](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi27.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi27.jpg)

加上[VestaCP](https://vestacp.com/)面板欲使用的port

[![vestacpi28](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi28.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi28.jpg)

再進入/usr/local/vesta/nginx/conf/編輯nginx.conf檔案



[![vestacpi29](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi29.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi29.jpg)

尋找

    
    # Vhost
     server {
     listen 8083;


將8083修改成所需的port即可儲存

[![vestacpi30](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi30.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi30.jpg)

重新啟動伺服器讓[VestaCP](https://vestacp.com/)面板新port生效

[![vestacpi31](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi31.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi31.jpg)


## 9.[VestaCP](https://vestacp.com/)面板對應網域DNS


再到網域DNS配置指向伺服器IP完成[VestaCP](https://vestacp.com/)建置

[![vestacpi32](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi32.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi32.jpg)


## 10.處理[VestaCP](https://vestacp.com/)子帳戶無法串接資料庫問題


建議管理員可以在資料庫選項新增一個空白資料庫，

主要防止子帳戶無法對資料庫進行連接的問題。

[![vestacpi33](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi33.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/vestacpi33.jpg)



[VestaCP](https://vestacp.com/)面板配置到此完成，

其他的附加應用可以訪問[VestaCP](https://vestacp.com/)論壇 [https://forum.vestacp.com/](https://forum.vestacp.com/)

比方說cPanel主機常見的一鍵安裝功能，

[VestaCP](https://vestacp.com/)面板同樣有腳本可以使用，請參考以下連結

[https://forum.vestacp.com/viewtopic.php?f=19&t=8523](https://forum.vestacp.com/viewtopic.php?f=19&t=8523)
