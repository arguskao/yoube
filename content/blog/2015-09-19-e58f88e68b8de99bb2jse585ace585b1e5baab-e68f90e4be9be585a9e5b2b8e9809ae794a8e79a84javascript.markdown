---
author: hotbaidu
date: 2015-09-19 01:10:20+00:00
draft: false
title: 又拍雲JS公共庫-提供兩岸通用的JavaScript
type: post
categories:
- 網路工具
tags:
- CDN
- SSL
---

又拍雲JS庫官網 [https://upcdn.b0.upaiyun.com](https://upcdn.b0.upaiyun.com)

[![chinacdnlibrary](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary.jpg)
](https://upcdn.b0.upaiyun.com/)

<!-- more -->由於近期有CDN廠商公布ICP備案網站的中國境內節點，有備案的站點在中國的速度可以提升不少，近期還會提供中國境內免費的SSL服務，中國境內的https網站要在海內外運作相對容易許多。

但是，一般網站通常有使用第三方的jQuery來源，
國際上有Google [https://developers.google.com/speed/libraries/](https://developers.google.com/speed/libraries/)
jQuery.com [https://code.jquery.com/](https://code.jquery.com/)
cdnjs.com [https://cdnjs.com/libraries](https://cdnjs.com/libraries)
中國境內內則有百度 [http://cdn.code.baidu.com/](http://cdn.code.baidu.com/)
360公共庫 [http://libs.useso.com/](http://libs.useso.com/)
大部分國外的來源在中國都無法連線，而中國境內來源則是造成海外訪問卡頓，除了百度公共庫最近新增香港機房外，又拍雲的公共庫也有提供海外各地的快取節點，這對於中國內外網站訪問速度有一定的加速效果。

對於https的網站來說，勢必無法使用百度、360公共庫，此時提供https的又拍雲就能應付這個困擾。

下方圖片顯示又拍雲在亞洲的反應時間，在中國上海3ms、台北5ms、九龍3ms都是非常快的反應時間。


使用maplatency檢測 [https://www.maplatency.com](https://www.maplatency.com)


[![chinacdnlibrary1](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary1.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary1.jpg)

在中國超級ping的結果可以看到許多機房，有網宿節點與電信節點，反應時間通常不超過15ms


使用超級ping檢測 [http://ping.chinaz.com](http://ping.chinaz.com)


[![chinacdnlibrary2](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary2.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary2.jpg)

接下來就是透過台灣中華20M非固定制線路追蹤路由，結果反應時間一樣只有23ms，IP為210.63.206.200

[![chinacdnlibrary3](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary3.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary3.jpg)

此時查詢210.63.206.200的IP資料，結果顯示主機位於台北的"亞太線上"機房，效果幾乎等同使用Google公共庫


使用IP-address.com檢測 [http://www.ip-adress.com/ip_tracer/](http://www.ip-adress.com/ip_tracer/)


[![chinacdnlibrary4](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary4.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary4.jpg)

由以上的結果可以了解到，如果網站使用的jQuery公共庫要讓中國內外都能順利訪問的話，又拍雲JS庫絕對是首選

到又拍雲JS庫官網點選jQuery檢視公共庫網址

[![chinacdnlibrary5](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary5.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary5.jpg)

目前提供的來源如下

//upcdn.b0.upaiyun.com/libs/jquery/jquery-2.0.3.min.js

//upcdn.b0.upaiyun.com/libs/jqueryui/jquery.ui-1.10.3.min.js

[![chinacdnlibrary6](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary6.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary6.jpg)

此時就可以到WordPress修改jQuery公共庫使用來源改用又拍雲，如果不熟悉修改方式的話可以使用外掛處理

Switch jQuery Version外掛官網 [https://wordpress.org/plugins/switch-jq-version/](https://wordpress.org/plugins/switch-jq-version/)

[![chinacdnlibrary7](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary7.jpg)
](https://wordpress.org/plugins/switch-jq-version/)

安裝後，首先點選"所使用的CDN"選項

[![chinacdnlibrary8](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary8.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary8.jpg)

改成"自定義"

[![chinacdnlibrary9](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary9.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary9.jpg)

再到又拍雲複製連結

並將開頭的http或https補齊，版本號則使用%VERSION%取代

[![chinacdnlibrary10](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary10.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary10.jpg)

範例 https://upcdn.b0.upaiyun.com/libs/jquery/jquery-%VERSION%.min.js

[![chinacdnlibrary11](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary11.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary11.jpg)

貼回Switch jQuery Version外掛，並將jQuery版本下調到2.0.3以下，又拍雲尚未提供2.0.3以上的版本

[![chinacdnlibrary12](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary12.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/chinacdnlibrary12.jpg)

最後請清理快取外掛即可生效，此時jQuery在中國內外載入都會非常迅速！



下一篇[耍賤的dts-net.com](https://www.yoube.net/2015/09/09/%e8%80%8d%e8%b3%a4%e7%9a%84dts-net-com/)
