---
author: admin
date: 2011-10-24 03:03:38+00:00
draft: false
title: 簡介wordpress
type: post
categories:
- 網路行銷課程
tags:
- wordpress
---

世界上的Blog最多的就是使用wordpress這個程式，為何會這麼受歡迎呢，免費當然是其中一個原因，但是wordpress有豐富的外掛，還有多到爆炸的版型，很多很複雜的功能都可以透過幾個按鈕就可以達成。

鑑於大家還沒有主機可以裝wordpress這個程式，先請大家去[wordpress.com](http://zh-tw.wordpress.com/)申請免費的來玩一下。就可以熟悉wordpress迷人在哪裡。

wordpress.com 因為是免費的，所以有一點限制，不能自己新增外掛，如果要新增的話，得付錢，不過我們之後會教大家用免費空間來玩一下wordpress，高級課程就會有，請繼續努力。

作業：請大家申請好免費的wordpress.com以後，把網址留給我，上面只要寫一篇文章就好，連結到你原來寫的pixnet網站，還有我的網站。

下一篇[如何申請vps架設自己的網站](http://www.yoube.net/2011/10/19/%e5%a6%82%e4%bd%95%e7%94%b3%e8%ab%8bvps%e6%93%81%e6%9c%89%e8%87%aa%e5%b7%b1%e7%9a%84%e7%b6%b2%e7%ab%99/)
