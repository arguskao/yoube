---
author: hotbaidu
date: 2016-06-30 16:48:37+00:00
draft: false
title: 網站HTTP headers配置-從訪客端提升站點安全性
type: post
categories:
- 網站建置
- 網路工具
- 虛擬主機
tags:
- SSL
- VPS
- 網站建置
- 網路工具
---

今年六月，KeyCDN官方部落格寫了一篇關於站點HTTP headers配置，
設定包括X-XSS-Protection、X-Frame-Options、X-Content-Type-Options
Content-Security-Policy、Strict-Transport-Security、Public-Key-Pins等六項。

KeyCDN部落格文章 [https://www.keycdn.com/blog](https://www.keycdn.com/blog/http-security-headers/)

站點的HTTP headers安全性可以透過securityheaders.io檢測平台進行初步測試，
但部分功能需要進一步修正才能發揮功效。

securityheaders.io官網 [https://securityheaders.io](https://securityheaders.io)

[![httpheader](https://www.yoube.net/wp-content/uploads/2016/07/httpheader.jpg)
](https://www.hzsh.xyz/877/網站http-headers配置-從訪客端提升站點安全性)<!-- more -->

檢測結果範例如下(各站點配置都會有差異)

[![httpheader1](https://www.yoube.net/wp-content/uploads/2016/07/httpheader1.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/httpheader1.jpg)


## **1.X-XSS-Protection、X-Frame-Options、X-Content-Type-Options配置如下**



    
    Nginx站點 .conf
    
    http {
     add_header X-XSS-Protection "1; mode=block";
     add_header X-Frame-Options SAMEORIGIN;
     add_header X-Content-Type-Options nosniff;
     }


Apache站點 .htaccess

    
    <IfModule mod_headers.c>
     Header set X-XSS-Protection "1; mode=block"
     Header set X-Frame-Options SAMEORIGIN env=!REDIRECT_IS_embed
     Header set X-Content-Type-Options nosniff
     </IfModule>




## **2.Content-Security-Policy配置範本如下**


Nginx站點 .conf

    
    http {
     add_header Content-Security-Policy "default-src 'self' https://sgised.r.worldssl.net ; script-src 'self' https://sgised.r.worldssl.net 'unsafe-inline' 'unsafe-eval' https://cdn.jsdelivr.net https://www.google-analytics.com; img-src 'self' https://sgised.r.worldssl.net data: https://ssl.google-analytics.com; connect-src 'self' https://sgised.r.worldssl.net https://ssl.google-analytics.com; style-src 'self' https://sgised.r.worldssl.net 'unsafe-inline' https://fonts.googleapis.com https://cdn.jsdelivr.net; font-src 'self' https://sgised.r.worldssl.net data: https://themes.googleusercontent.com https://cdn.jsdelivr.net; frame-src 'self' https://sgised.r.worldssl.net; object-src 'none'";
     }


Apache站點 .htaccess

    
    <IfModule mod_headers.c>
     Header set Content-Security-Policy "default-src 'self' https://sgised.r.worldssl.net ; script-src 'self' https://sgised.r.worldssl.net 'unsafe-inline' 'unsafe-eval' https://cdn.jsdelivr.net https://www.google-analytics.com; img-src 'self' https://sgised.r.worldssl.net data: https://ssl.google-analytics.com; connect-src 'self' https://sgised.r.worldssl.net https://ssl.google-analytics.com; style-src 'self' https://sgised.r.worldssl.net 'unsafe-inline' https://fonts.googleapis.com https://cdn.jsdelivr.net; font-src 'self' https://sgised.r.worldssl.net data: https://themes.googleusercontent.com https://cdn.jsdelivr.net; frame-src 'self' https://sgised.r.worldssl.net; object-src 'none'"
     </IfModule>


Content-Security-Policy分成default-src、script-src、img-src
connect-src、style-src、font-src、frame-src、object-src等標籤，
須將網站素材對應的網址分別輸入Content-Security-Policy內容，
請留意Content-Security-Policy配置錯誤將會造成頁面部分內容無法載入的問題。

可以在Chrome瀏覽器按下F12檢查Console內容是否有被封鎖的來源，
再新增到Content-Security-Policy即可。

[![httpheader5](https://www.yoube.net/wp-content/uploads/2016/07/httpheader5.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/httpheader5.jpg)

參考教學 [https://developer.mozilla.org](https://developer.mozilla.org/zh-CN/docs/Web/Security/CSP/CSP_policy_directives)


## **3.Strict-Transport-Security(簡稱HSTS)配置如下**


Nginx站點 .conf

    
    http {
    add_header Strict-Transport-Security "max-age=15552000";
    }


Apache站點 .htaccess

    
    <IfModule mod_headers.c>
    Header set Strict-Transport-Security "max-age=15552000" env=HTTPS
    </IfModule>


根據[Qualys SSL Labs](https://www.ssllabs.com/ssltest/index.html)規定，HSTS時間必須長達180天(max-age=15552000)

Public-Key-Pins(簡稱HPKP)配置方法需要兩樣工具，
一個是用於檢測SSL證書(.crt)的pin-sha256，
另一個為沃通免費SSL憑證簽發服務，將用來生成相同網域的SSL憑證，
作為備用pin-sha256的產生來源。

SSL憑證pin-sha256檢測腳本 [https://github.com/DavisNT/Public-Key-Pins](https://github.com/DavisNT/Public-Key-Pins)

[![httpheader2](https://www.yoube.net/wp-content/uploads/2016/07/httpheader2.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/httpheader2.jpg)

請直接下載zip檔案到本地解壓縮，瀏覽器打開資料夾中calculator.html，
即可貼上SSL憑證的.crt檔案計算pin-sha256。

沃通WoSign免費SSL憑證官網 [https://buy.wosign.com/free](https://buy.wosign.com/free)

[![httpheader4](https://www.yoube.net/wp-content/uploads/2016/07/httpheader4.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/httpheader4.jpg)

請透過英文版站點免費申請網域的三年SSL憑證，
需留意免費SSL最多只能附加四組網域到同一張SSL憑證。


## **4.Public-Key-Pins配置範本如下**


Nginx站點 .conf

    
    http {
    add_header Public-Key-Pins 'pin-sha256="X3pGTSOuJeEVw989IJ/cEtXUEmy52zs1TZQrU06KUKg="; \
    pin-sha256="MHJYVThihUrJcxW6wcqyOISTXIsInsdj3xK8QrZbHec="; \
    max-age=2592000';
    }


Apache站點 .htaccess

    
    <IfModule mod_headers.c>
    Header set Public-Key-Pins "pin-sha256=\"Vpu6s42fQkQS6Pu++Z8uHPjYfX4SIFD8UdyLMYOdgPo=\"; pin-sha256=\"U/lRHz44FbjgFAIxCDk30Rj3YbqJAeP8wz381DJv+eI=\"; max-age=2592000"
    </IfModule>


HPKP是透過瀏覽器端檢測站點SSL憑證是否被替換過，
根據securityheaders.io規定HPKP時間必須長達30天(max-age=2592000)，
但部分瀏覽器會將HPKP與HSTS的時間混淆，
若環境允許，請將HPKP時間延長到180天(max-age=15552000)，
由於HPKP是判斷SSL憑證是否一致，
不適合短期的SSL憑證，包括[Let's Encrypt](https://letsencrypt.org/)、[CF SSL](https://www.cloudflare.com/ssl/)、[百度雲加速专业版](https://su.baidu.com)SSL，
更換憑證時容易出問題。

站點Public-Key-Pins配置檢測工具 [https://report-uri.io/home/pkp_analyse](https://report-uri.io/home/pkp_analyse)

[![httpheader3](https://www.yoube.net/wp-content/uploads/2016/07/httpheader3.jpg)
](https://www.yoube.net/wp-content/uploads/2016/07/httpheader3.jpg)

鍵入您站點的網域(須包括https://開頭)，
會檢測站點當前使用的SSL與第一組pin-sha256是否一致，
再檢測第二組pin-sha256是否與當前網域符合，不能與第一組重複，
驗證通過瀏覽器才能正確應用HPKP的安全配置。
