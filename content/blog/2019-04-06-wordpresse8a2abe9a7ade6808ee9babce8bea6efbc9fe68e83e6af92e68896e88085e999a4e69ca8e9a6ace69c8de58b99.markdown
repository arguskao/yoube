---
author: admin
date: 2019-04-06 03:11:59+00:00
draft: false
title: Wordpress被駭怎麼辦？掃毒或者除木馬服務
type: post
categories:
- 接案相關
- 網站建置
tags:
- wordpress
---

以前大概一年會接到三四攤[wordpress](https://www.yoube.net/tag/wordpress)中毒的案件，現在是幾乎每個月都會有，我想應該不是我們SEO做的好，而是因為Wordpress的使用頻率增加很多，但是大家的使用觀念並沒有隨之更新，所以中毒或者是中木馬的機率大增。

至於被駭或者中毒的原因大概可以分成幾項


## 內部





 	  1. 同事外洩（例如在公眾電腦登入，不要笑，這個很常見，包括使用公用Wifi）
 	  2. 上網的那台電腦有問題（已經中毒或者木馬）
 	  3. 虛擬主機中毒，然後開始蔓延（這種情形非常普遍，很多主機公司本身防護不足）
 	  4. 主機沒有防火牆（這幾年很多人愛用VPS，卻不懂架設防火牆）
 	  5. 程式本身問題（沒有即時更新，漏洞百出）
 	  6. 使用免費外掛，中間有藏毒或者木馬（高達一半都是這種情況）



## 外部





 	  1. 被暴力破解（這個很不可思議，但常發生）
 	  2. 釣魚程式



中了毒或者被駭怎麼辦？

 	  1. 如果是自己玩的部落格，不用急，上網找資料或者社團求助
 	  2. 如果是購物網站，有做生意的，衡量一下每天損失的營業額，可以考慮專業的公司來幫忙




