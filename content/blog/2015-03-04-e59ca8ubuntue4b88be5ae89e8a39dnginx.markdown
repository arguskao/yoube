---
author: admin
date: 2015-03-04 08:21:50+00:00
draft: false
title: 在Ubuntu下安裝Nginx+HHVM+Mysql
type: post
categories:
- 網站建置
tags:
- VPS
- wordpress
- 虛擬主機
---

不知道為何Nginx就開始流行起來了，因為相比較起Apache來，它的確是可以輕盈許多，對於現在還是不怎麼便宜的[VPS](https://www.yoube.net/tag/vps/)來說，可以省下不少記憶體。

如果是使用Ubuntu的網友，可參考這樣的裝法，就可以直接裝[Wordpress](https://www.yoube.net/tag/wordpress/)來玩了。

如果要裝stable版本

sudo apt-get update
sudo apt-get install python-software-properties software-properties-common
sudo add-apt-repository ppa:nginx/stable
sudo apt-get update
sudo apt-get -y install nginx

如果要裝最新版本的話

sudo apt-get update
sudo apt-get install python-software-properties software-properties-common
sudo add-apt-repository -s -y ppa:nginx/development
sudo apt-get update
sudo apt-get -y install nginx

然後再裝HHVM（假設ubuntu 14.04)，版本不同的話，請更改紅色部分


wget -0 - http://dl.hhvm.com/conf/hhvm.gpg.key | sudo apt-key add -


echo deb http://dl.hhvm.com/ubuntu trusty main | sudo tee /etc/apt/sources.list.d/hhvm.list
sudo apt-get update
sudo apt-get install -y hhvm
sudo /usr/share/hhvm/install_fastcgi.sh
sudo update-rc.d hhvm defaults
sudo service hhvm restart

根據我的實驗，可以不用另外安裝php-fpm，好像也不特別會當機，可能是我的流量小的關係。

然後需要安裝mysql，這裡用MariaDB取代

sudo apt-get install mariadb-server mariadb-client

切記，如果要使用phpmyadmin，要安裝php5-mysql，而且要下載原始檔安裝，不要直接apt-get install



下一篇[美國虛擬主機interserv](https://www.yoube.net/2015/02/19/%e7%be%8e%e5%9c%8b%e8%99%9b%e6%93%ac%e4%b8%bb%e6%a9%9finterserver/)
