---
author: hotbaidu
date: 2015-09-07 14:55:14+00:00
draft: false
title: Adblock Browser for Android-可隱藏廣告的安卓瀏覽器
type: post
categories:
- 網路工具
tags:
- adsense
- Adwords
- 免費
---

9/8更新 Adblock Browser for Android正式發布，不需加入測試人員即可下載安裝
Adblock瀏覽器下載官網 https://adblockbrowser.org/zh-TW/

![](https://www.yoube.net/wp-content/uploads/2015/09/adblockb17.jpg)


<!-- more -->Adblock Browser Beta官網 [https://adblockplus.org/en/adblock-browser/](https://adblockplus.org/en/adblock-browser/)

[![adblockb](https://www.yoube.net/wp-content/uploads/2015/09/adblockb.jpg)
](https://adblockplus.org/en/adblock-browser/)
在電腦版瀏覽器有隱藏廣告外掛[Adblock Plus](https://adblockplus.org/zh_TW/)，點選幾下即可順利安裝，並忽略所有惱人的廣告，但對於同樣能上網的智慧型手機來說並不容易，往常須透過Root或Proxy才能移除網頁廣告，缺點是前者不一定適用於所有Android手機、後者完全不適用於處理https網頁的廣告。幸好，今年[Adblock Plus](https://adblockplus.org/zh_TW/)計畫開發內建隱藏廣告的手機瀏覽器，Android系統用戶可以搶先體驗，除了透過已發布的APK進行安裝外，也能透過下方介紹的方式以GooglePlay安裝，未來更新程式會更加便捷。

進入[Adblock Browser](https://adblockplus.org/en/adblock-browser/)公測官網後，點選"[Beta test Adblock Browser for Android](https://adblockplus.org/redirect?link=adblock_browser_android_beta_community)"進入[Google+社團](https://plus.google.com/communities/104936844759781288661)

請點選"加入社團"

[![adblockb1.jpg](https://www.yoube.net/wp-content/uploads/2015/09/adblockb1.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb1.jpg)

加入社團成功後，下拉畫面至"關於這個社團"，點選"BETA DOWNLOAD"進入GooglePlay

[![adblockb2](https://www.yoube.net/wp-content/uploads/2015/09/adblockb2.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb2.jpg)

此時點選"成為測試人員"，即可成功加入[Adblock Browser](https://adblockplus.org/en/adblock-browser/)的測試計畫

[![adblockb3](https://www.yoube.net/wp-content/uploads/2015/09/adblockb3.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb3.jpg)

此時即可在Android裝置進入GooglePlay安裝Adblock Browser for Android

[![adblockb4](https://www.yoube.net/wp-content/uploads/2015/09/adblockb4.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb4.jpg)

本瀏覽器內核為Firefox，目前尚未提供類似於Firefox Sync的同步功能

[![adblockb5](https://www.yoube.net/wp-content/uploads/2015/09/adblockb5.jpg)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb5.jpg)

安裝完畢後，進入瀏覽器請先開啟"設定"選單，我們先配置隱藏策略的完整性

[![adblockb6](https://www.yoube.net/wp-content/uploads/2015/09/adblockb6.png)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb6.png)

點選"廣告過濾"選項

[![adblockb7](https://www.yoube.net/wp-content/uploads/2015/09/adblockb7.png)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb7.png)

點選"可接受廣告"

[![adblockb8](https://www.yoube.net/wp-content/uploads/2015/09/adblockb8.png)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb8.png)

允許一些非入侵式廣告，使用者可以自行評估是否勾選，官方建議用戶勾選本功能

[![adblockb9](https://www.yoube.net/wp-content/uploads/2015/09/adblockb9.png)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb9.png)

現在可以進行瀏覽體驗廣告隱藏效果，連https連線的廣告均可以處理，對於時常顯示全屏廣告的新聞網站來說最為合適，也能有效地延長手機的續航力，一舉數得。目前唯一遇到的困擾是，瀏覽器對於網頁捲動的效果不夠流暢，未來更新應該會有所改善。

同場加映：將Google網頁翻譯功能加入書籤中

搜尋"翻譯瀏覽器按鈕"或點選網址 [http://translate.google.com/translate_buttons](http://translate.google.com/translate_buttons)

[![adblockb11](https://www.yoube.net/wp-content/uploads/2015/09/adblockb11.png)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb11.png)

進入官網後長按或右鍵"中文(繁體)"

[![adblockb12](https://www.yoube.net/wp-content/uploads/2015/09/adblockb12.png)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb12.png)

選擇"將鏈結加入書籤"即可

[![adblockb13](https://www.yoube.net/wp-content/uploads/2015/09/adblockb13.png)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb13.png)

未來遇到要翻譯的網頁，點選網址欄

[![adblockb14](https://www.yoube.net/wp-content/uploads/2015/09/adblockb14.png)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb14.png)

選擇"書籤"中"中文(繁體)"

[![adblockb15](https://www.yoube.net/wp-content/uploads/2015/09/adblockb15.png)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb15.png)

即可透過[Google翻譯](https://translate.google.com.tw)網頁

[![adblockb16](https://www.yoube.net/wp-content/uploads/2015/09/adblockb16.png)
](https://www.yoube.net/wp-content/uploads/2015/09/adblockb16.png)

如果不喜歡Google網頁翻譯的服務，
也能換口味嘗試微軟的[Bing翻譯](http://www.bing.com/translator) [http://labs.microsofttranslator.com/bookmarklet/](http://labs.microsofttranslator.com/bookmarklet/)





下一篇[耍流氓的七雲虛擬主機](https://www.yoube.net/2015/09/07/%e8%80%8d%e6%b5%81%e6%b0%93%e7%9a%84%e4%b8%83%e9%9b%b2%e8%99%9b%e6%93%ac%e4%b8%bb%e6%a9%9f/)
