---
author: hotbaidu
date: 2016-03-13 12:07:07+00:00
draft: false
title: Incapsula網站加速設置-免費優質CDN服務
type: post
categories:
- 網站建置
- 虛擬主機
tags:
- CDN
- cdn教學
- 免費
---

[Incapsula](https://www.incapsula.com/)提供與[CloudFlare](https://www.cloudflare.com/)類似的網頁安全與加速服務，
但沒有強迫代管網域NS配置，
僅須修改DNS的A紀錄與CNAME紀錄即可完成CDN與DDos防護配置。

這裡暫時不推薦使用[Incapsula](https://www.incapsula.com/)的付費服務，
主要是官網報價雖然不限流量，卻有限制網速必須小於5Mbps，
超過將會直接加收費用，等於如果流量一來報價無上限。

須留意免費版[Incapsula](https://www.incapsula.com/)只適合不需使用SSL的站點，
與[CloudFlare](https://www.cloudflare.com/)一樣，當站點被攻擊時帳戶將被警告，
同樣抗DDos方案，還是建議選[CloudFlare](https://www.cloudflare.com/)的200美金方案比較划算。

Incapsula官網 [https://www.incapsula.com/](https://www.incapsula.com/)

[![incapsula](https://www.yoube.net/wp-content/uploads/2016/03/incapsula.jpg)
](https://www.incapsula.com/)<!-- more -->


## 1.申請[Incapsula](https://www.incapsula.com/)免費CDN服務


進入官網，按下PRICING&SIGN UP申請服務

[![incapsula2](https://www.yoube.net/wp-content/uploads/2016/03/incapsula2.jpg)
](https://www.incapsula.com/pricing-and-plans.html)

按下Free方案的START NOW進入帳戶申請畫面

[![incapsula3](https://www.yoube.net/wp-content/uploads/2016/03/incapsula3.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula3.jpg)

輸入您的姓名、信箱地址、要使用的密碼

[![incapsula4](https://www.yoube.net/wp-content/uploads/2016/03/incapsula4.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula4.jpg)


## 2.添加站點到[Incapsula](https://www.incapsula.com/)CDN服務


驗證好[Incapsula](https://www.incapsula.com/)帳戶後，在頁面輸入您的網域
(由於之前申請過服務，顯示的畫面與您初次申請有些不同)

在按下+Add Website

[![incapsula5](https://www.yoube.net/wp-content/uploads/2016/03/incapsula5.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula5.jpg)

驗證網域確認沒有https的問題，請按下Continue繼續

[![incapsula6](https://www.yoube.net/wp-content/uploads/2016/03/incapsula6.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula6.jpg)

顯示[Incapsula](https://www.incapsula.com/)提供的站點A紀錄與CNAME紀錄

[![incapsula7](https://www.yoube.net/wp-content/uploads/2016/03/incapsula7.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula7.jpg)

請將紀錄分別加入您網域的DNS紀錄

如果是使用[CloudFlare](https://www.cloudflare.com/)DNS的站長們請留意，
必須將橘色的雲取消(保持灰色)讓[CloudFlare](https://www.cloudflare.com/)的CDN失效即可

[![incapsula8](https://www.yoube.net/wp-content/uploads/2016/03/incapsula8.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula8.jpg)


## 3.[Incapsula](https://www.incapsula.com/)站點狀態監測與調整


過不久DNS解析完成後即可到[Incapsula](https://www.incapsula.com/)查看網站加速情形

[![incapsula9](https://www.yoube.net/wp-content/uploads/2016/03/incapsula9.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula9.jpg)

這裡是網站流量的狀態，可以看到訪客來源與頻寬使用的大小

點選Settings設定站點的加速配置

[![incapsula10](https://www.yoube.net/wp-content/uploads/2016/03/incapsula10.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula10.jpg)

這裡是設定原始主機的IP或CNAME(需留意CNAME無法隨意更改，建議都以IP設定)

[![incapsula11](https://www.yoube.net/wp-content/uploads/2016/03/incapsula11.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula11.jpg)

點選General取消Web Seal(否則網站將出現右下角的[Incapsula](https://www.incapsula.com/)圖示)

[![incapsula12](https://www.yoube.net/wp-content/uploads/2016/03/incapsula12.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula12.jpg)

接下來點選Security

[![incapsula13](https://www.yoube.net/wp-content/uploads/2016/03/incapsula13.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula13.jpg)

勾選Require all other Suspected Bots to pass a CAPTCHA test並Save儲存

[![incapsula14](https://www.yoube.net/wp-content/uploads/2016/03/incapsula14.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula14.jpg)

如果需要封鎖特定國家可以在Block Countries設定

[![incapsula15](https://www.yoube.net/wp-content/uploads/2016/03/incapsula15.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula15.jpg)

直接點選國家即可

[![incapsula16](https://www.yoube.net/wp-content/uploads/2016/03/incapsula16.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula16.jpg)

如果要完全允許某個國家訪問，請點選Block Countries右下角的Add exception

[![incapsula17](https://www.yoube.net/wp-content/uploads/2016/03/incapsula17.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula17.jpg)

選擇國家並輸入國家名稱即可

[![incapsula18](https://www.yoube.net/wp-content/uploads/2016/03/incapsula18.jpg)
](https://www.yoube.net/wp-content/uploads/2016/03/incapsula18.jpg)

基本設定到此完成。

[Incapsula](https://www.incapsula.com/)在台灣最大的優勢在於亞洲地區都是使用[NTT](https://zh.wikipedia.org/w/index.php?title=日本電信電話&oldid=38844016)線路，

優勢是台灣接入[NTT](https://zh.wikipedia.org/w/index.php?title=日本電信電話&oldid=38844016)的頻寬大，

不像[CloudFlare](https://www.cloudflare.com/)的[Telstra](https://zh.wikipedia.org/w/index.php?title=澳大利亞電信&oldid=39194351)線路接入台灣Hinet易壅塞，

適合訪客在台灣且不需要https連接的站點，

如果訪客在海外、或是站點有SSL需求，[CloudFlare](https://www.cloudflare.com/)是首選，

再加上如果有DDos防護需求，[CloudFlare](https://www.cloudflare.com/)不會依攻擊洪峰加價，

這方面比[Incapsula](https://www.incapsula.com/)還要有優勢。
