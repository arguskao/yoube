---
title: "關於公司"
date: 2018-12-27T09:10:27+06:00
type: "post"
---

<h5>一般公司會告訴你，他們什麼都會，但是我們公司只專精於一個產品就是WordPress。</h4>

<h5>以下是我們的服務項目：</p>

<ol>1.各種Wordpress疑難雜症，包括從Pixnet搬家</ol>
<ol>2.主機租賃，代管維護。</ol>
<ol>4.代為設定VPS或者管理獨立主機。</ol>
<ol>5.SEO(網頁搜尋結果排前）</ol>
<ol>6.優化網頁速度</ol>
<ol>7.提昇PageSpeed分數</ol>
<ol>8.投放Google或者臉書廣告</ol>
<ol>9.管理粉專</ol>

<h5>我們不是一般Soho，做完以後沒有保固，也不是沒有規模的公司，請上經濟部查詢，我們是實收資本四百萬的公司，您可以安心把工作交給我們。

<h5>如果您有網站設計的需求，又不想花太多預算，請交給我們。如果你是網站孤兒，更要找我們，幫您處理大小事務，讓您花小錢大享受。

<h3><a href="tel:+88662678105">來電洽詢：06-2678105</a><br/>
市話請撥0800-000-783

<p><p><br/>

<center><a href="https://line.me/R/ti/p/@rid4480s" target="_blank" rel="noopener"><img class="size-full" src="https://i.imgur.com/epiisSh.png" alt="加入好友" width="232" height="72" border="0">


