---
title: "我們的優勢"
date: 2018-12-28T11:02:05+06:00
icon: "ti-package"
description: "我們可能不是能力最強，但一定是服務最好"
type : "pages"
---

<h5>坊間很多公司，都是業務一堆，程式工程師少</h5>

<h5>甚至還有全部外包的!</h5>

<h5>萬一客戶有問題就無法立即得到回應</h5>


<h5>我聽過裝一個SSL，需時一個月，還有客戶居然會相信是因為公司生意太好的鬼話。</h4>


<h5>我們不貪接客戶，只希望能給客戶最好的服務</h5>

<h4><font color=red>我們承諾一年365天都讓你找到人，兩小時內，必定給客戶回應。</font></h3>
<p><p><br/>

<center><a href="https://line.me/R/ti/p/@rid4480s" target="_blank" rel="noopener"><img class="size-full" src="https://i.imgur.com/epiisSh.png" alt="加入好友" width="232" height="72" border="0">